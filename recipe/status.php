<?php

namespace Deployer;

use Symfony\Component\Console\Helper\Table;

task('status', function () {
    cd('{{current_path}}');

    // Doesn't work locally
    set('env', []);

    // Update
    runLocally('git fetch origin');

    $branch = run('git rev-parse --abbrev-ref HEAD');
    $commit = run('git rev-parse HEAD');

    // Show commits behind remote branch
    $behind = runLocally(sprintf('git rev-list origin/%s --not %s | wc -l', $branch, $commit));

    $table = new Table(output());

    $table->setHeaders(['Host', 'Branch', 'Commit', 'Commit(s) behind']);
    $table->addRow([get('hostname'), $branch, substr($commit, 0, 7), $behind]);

    $table->render();
})->desc('Show the status of the given stage');
