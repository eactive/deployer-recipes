<?php

namespace Deployer;

use Symfony\Component\Yaml\Inline;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Yaml;

task('deploy:build_parameters', function () {
    $dir = get('release_path');

    $parametersFile = get('parameters_file', $dir . '/app/config/parameters.yml');
    $distFile = get('parameters_dist_file', $parametersFile . '.dist');

    $parser = new Parser();

    $parameterKey = 'parameters';
    $actualParams = [];

    if (test(sprintf('[ -f %s ]', $parametersFile))) {
        $actualParams = (array) $parser->parse(run('cat ' . $parametersFile));
    }

    if (!isset($actualParams[$parameterKey])) {
        $actualParams = [$parameterKey => []];
    }

    if (!test(sprintf('[ -f %s ]', $distFile))) {
        writeln(sprintf('<comment>The dist file "%s" does not exist.</comment>', $distFile));
        return 0;
    }

    $expectedParams = (array) $parser->parse(run('cat ' . $distFile));

    if (!isset($expectedParams[$parameterKey])) {
        writeln(sprintf('<comment>The top-level key %s is missing.</comment>', $parameterKey));
        return 0;
    }

    $started = false;

    foreach ($expectedParams[$parameterKey] as $key => $message) {
        if (array_key_exists($key, $actualParams[$parameterKey])) {
            continue;
        }

        if (!$started) {
            $started = true;
            writeln('<comment>Some parameters are missing. Please provide them.</comment>');
        }

        $default = Inline::dump($message);
        $value = ask($key, $default);

        $actualParams[$parameterKey][$key] = Inline::parse($value);
    }

    $escape = [
        '"' => '\"', // double quotes
        '`' => '\`', // backtick operator
        '$' => '\$', // arguments
    ];

    $parameters = str_replace(array_keys($escape), array_values($escape), Yaml::dump($actualParams, 99));

    run(sprintf('echo "%s" > %s', $parameters, $parametersFile));
})->desc('Support build parameters');
