<?php

namespace Deployer;

use Deployer\Task\Context;
use Throwable;

task('deploy:branch', function () {
    try {
        cd('{{current_path}}');
    } catch (Throwable $exception) {
        // Current path not found
        return 0;
    }

    $env = get('env');

    // Doesn't work locally
    set('env', []);

    // Update and cleanup
    runLocally('git fetch origin -p');

    // List remote branches
    $branches = array_map('trim', preg_split('/\n/', preg_replace('/[ ]{0,}origin\//', '', runLocally('git branch -r | grep origin/'))));

    // Filter non-revelant branches
    $branches = array_values(array_filter($branches, function($branch) {
        if (!preg_match('/^(HEAD ->.+|master)$/', $branch)) {
            return $branch;
        }
    }));

    // Master should be first
    array_unshift($branches, 'master');

    // Try to set current remote branch as default
    $branch = run('git rev-parse --abbrev-ref HEAD');
    $default = array_search($branch, $branches);

    if ($default === false) {
        $default = null;
    }

    if (!input()->getOption('branch') && !Context::get()->getConfig()->has('branch')) {
        // Only ask if not provided
        set('branch', askChoice('Which branch do you want to deploy?', $branches, $default));
    }

    // Show commits behind remote master
    $behind = runLocally('git rev-list origin/master --not origin/{{branch}} | wc -l');

    if ($behind) {
        writeln(sprintf('<comment>Branch {{branch}} is %d commit(s) behind master</comment>', $behind));
    }

    // Restore
    set('env', $env);
})->desc('Ask for the branch to deploy and show if it is behind master');
